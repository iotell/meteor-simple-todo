/* eslint-env mocha */
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
// import { Factory } from 'meteor/dburles:factory';
// import Fake from 'meteor/anti:fake';
import sinon from 'sinon';

import { Tasks } from './tasks.js';

// General functions for the tests
// Factory.define('user', Meteor.users, {
//   _id: Random.id(),
//   username: 'test user',
// });

// Tests on server
if (Meteor.isServer) {
  describe('Tasks', () => {
    // Methods: delete
    describe('methods: delete', () => {
      const userId = Random.id();
      let taskId;

      beforeEach(() => {
        Tasks.remove({});
        taskId = Tasks.insert({
          text: 'test task',
          createdAt: new Date(),
          owner: userId,
          username: 'thomasmuster',
        });
      });

      it('can delete owned task', () => {
        const deleteTask = Meteor.server.method_handlers['tasks.remove'];
        const invocation = { userId };
        deleteTask.apply(invocation, [taskId]);
        assert.equal(Tasks.find().count(), 0);
      });

      it('cannot delete tasks of other users', () => {
        const deleteTask = Meteor.server.method_handlers['tasks.remove'];
        const invocation = { userId: Random.id() };
        try {
          deleteTask.apply(invocation, [taskId]);
        } catch (error) {
          assert.instanceOf(error, Meteor.Error);
        }
      });
    });

    // Methods: setChecked
    describe('methods: setChecked', () => {
      const userId = Random.id();
      let taskId;

      beforeEach(() => {
        // Set a standard
        Tasks.remove({});
        taskId = Tasks.insert({
          text: 'test task',
          createdAt: new Date(),
          owner: userId,
          username: 'thomasmuster',
          checked: undefined,
        });
      });

      it('can check owned private task', () => {
        Tasks.update(taskId, { $set: { private: true } });

        const setChecked = Meteor.server.method_handlers['tasks.setChecked'];
        const invocation = { userId };
        setChecked.apply(invocation, [taskId, true]);

        const task = Tasks.findOne(taskId);

        assert.isTrue(task.checked);
      });

      it('can uncheck owned public task', () => {
        Tasks.update(taskId, { $set: { private: false } });

        const setChecked = Meteor.server.method_handlers['tasks.setChecked'];
        const invocation = { userId };
        setChecked.apply(invocation, [taskId, false]);

        const task = Tasks.findOne(taskId);

        assert.isFalse(task.checked);
      });

      it('can check someone elses public task', () => {
        Tasks.update(taskId, { $set: { private: false } });

        const setChecked = Meteor.server.method_handlers['tasks.setChecked'];
        const invocation = { userId: Random.id() };
        setChecked.apply(invocation, [taskId, true]);

        const task = Tasks.findOne(taskId);

        assert.isTrue(task.checked);
      });

      it('cannot check someone elses private task', () => {
        Tasks.update(taskId, { $set: { private: true } });

        const setChecked = Meteor.server.method_handlers['tasks.setChecked'];
        const invocation = { userId: Random.id() };

        try {
          setChecked.apply(invocation, [taskId, true]);
        } catch (error) {
          assert.instanceOf(error, Meteor.Error);
        }
      });
    });

    // Methods: setPrivate
    describe('methods: setPrivate', () => {
      const userId = Random.id();
      let taskId;

      beforeEach(() => {
        // Set a standard
        Tasks.remove({});
        taskId = Tasks.insert({
          text: 'test task',
          createdAt: new Date(),
          owner: userId,
          username: 'thomasmuster',
          private: undefined,
        });
      });

      it('can set owned task', () => {
        const setPrivate = Meteor.server.method_handlers['tasks.setPrivate'];
        const invocation = { userId };
        setPrivate.apply(invocation, [taskId, true]);

        const task = Tasks.findOne(taskId);

        assert.isTrue(task.private);
      });

      it('cannot set someone elses task', () => {
        const setPrivate = Meteor.server.method_handlers['tasks.setPrivate'];
        const invocation = { userId: Random.id() };

        try {
          setPrivate.apply(invocation, [taskId, true]);
        } catch (error) {
          assert.instanceOf(error, Meteor.Error);
        }
      });
    });

    // Methods: insert
    describe('methods: insert', () => {
      const currentUser = Fake.user();
      const userId = Random.id();

      beforeEach(() => {
        sinon.stub(Meteor, 'user');
        Meteor.user.returns(currentUser);
        Tasks.remove({});
      });

      afterEach(() => {
        if (Meteor.user.restore) Meteor.user.restore();
      });

      it('insert task when logged in', () => {
        const insert = Meteor.server.method_handlers['tasks.insert'];
        const invocation = {
          userId,
        };
        insert.apply(invocation, ['a todo text']);

        const task = Tasks.findOne({});

        assert.equal(task.text, 'a todo text');
      });

      it('cannot insert task when not logged in - no userId', () => {
        const insert = Meteor.server.method_handlers['tasks.insert'];
        const invocation = {};

        try {
          insert.apply(invocation, ['a todo text']);
        } catch (error) {
          assert.instanceOf(error, Meteor.Error);
        }
      });

      it('cannot insert task when not logged in - no user', () => {
        const insert = Meteor.server.method_handlers['tasks.insert'];
        const invocation = {
          userId,
        };
        Meteor.user.returns({});

        try {
          insert.apply(invocation, ['a todo text']);
        } catch (error) {
          assert.instanceOf(error, Meteor.Error);
        }
      });
    });
  });
}
