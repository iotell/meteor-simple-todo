import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Tasks = new Mongo.Collection('tasks');

// Server only code
if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('tasks', function tasksPublication() {
    return Tasks.find({
      $or: [{ private: { $ne: true } }, { ownerId: this.owner }],
    });
  });
}

Meteor.methods({
  // Insert
  'tasks.insert'(text) {
    check(text, String);

    checkLoggedIn(this.userId);

    Tasks.insert({
      text,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.user().username,
    });
  },

  // Delete
  'tasks.remove'(taskId) {
    check(taskId, String);

    const task = Tasks.findOne(taskId);
    checkOwner(task, this.userId);
    Tasks.remove(taskId);
  },

  // Set checked
  'tasks.setChecked'(taskId, setChecked) {
    check(taskId, String);
    check(setChecked, Boolean);

    const task = Tasks.findOne(taskId);
    checkPrivacy(task, this.userId);
    Tasks.update(taskId, { $set: { checked: setChecked } });
  },

  // Set private
  'tasks.setPrivate'(taskId, setToPrivate) {
    check(taskId, String);
    check(setToPrivate, Boolean);

    const task = Tasks.findOne(taskId);
    checkOwner(task, this.userId);
    Tasks.update(taskId, { $set: { private: setToPrivate } });
  },
});

/**
 * Checks whether the user is logged in.
 */
function checkLoggedIn(userId) {
  if (!userId) {
    throw new Meteor.Error('not-authorized');
  }
}

/**
 * Checks whether the current user is the owner of the task.
 */
function checkOwner(task, userId) {
  if (task.owner !== userId) {
    throw new Meteor.Error('not-authorized');
  }
}

/**
 * Checks whether the task is a private task.
 * If so only the owner of the task may pass this check.
 */
function checkPrivacy(task, userId) {
  if (task.private && task.owner !== userId) {
    throw new Meteor.Error('not-authorized');
  }
}
