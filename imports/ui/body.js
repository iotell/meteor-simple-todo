import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Tasks } from '../api/tasks.js';

import './task.js';
import './body.html';

Template.body.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
  Meteor.subscribe('tasks');
});

Template.body.helpers({
  tasks() {
    const instance = Template.instance();
    let queries = [];

    // If hide completed is checked, filter tasks
    if (instance.state.get('hideCompleted')) {
      queries.push({ checked: { $ne: true } });
    }

    // Only show own tasks
    if (instance.state.get('hideOthers')) {
      queries.push({ owner: Meteor.userId() });
    }

    // Otherwise, return all of the tasks
    let query = {};
    if (queries.length > 0) {
      query = { $and: queries };
    }
    return Tasks.find(query, { createdAt: 1 });
  },
  incompleteCount() {
    return Tasks.find({ checked: { $ne: true } }).count();
  },
  isLoggedIn() {
    return !!Meteor.userId();
  },
});

Template.body.events({
  'submit .new-task'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    // Insert a task into the collection
    Meteor.call('tasks.insert', text);

    // Clear form
    target.text.value = '';
  },
  'change .hide-completed input'(event, instance) {
    instance.state.set('hideCompleted', event.target.checked);
  },
  'change .hide-others input'(event, instance) {
    instance.state.set('hideOthers', event.target.checked);
  },
});
