import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './task.html';

Template.task.helpers({
  isOwner() {
    return this.owner === Meteor.userId();
  },
  disableByPrivacy() {
    // If private, then only the owner is ok.
    if (this.private) {
      return this.owner !== Meteor.userId() ? { disabled: 'disabled' } : {};
    }

    // Otherwise its ok.
    return {};
  },
});

Template.task.events({
  // Toggle checked
  'click .toggle-checked'() {
    // Set the checked property to the opposite of its current value
    Meteor.call('tasks.setChecked', this._id, !this.checked, (error, result) => {
      if (error) {
        // TODO ANDI. In order to see error-handling in action: allways enable the checkbox in the ui and implement a better errorhandler. For example a global snackbar / toast.
        alert('You are not authorized to change the checked, because it is private.');
      }
    });
  },

  // Delete task
  'click .delete'() {
    Meteor.call('tasks.remove', this._id);
  },

  // Toggle private
  'click .toggle-private'() {
    Meteor.call('tasks.setPrivate', this._id, !this.private);
  },
});
