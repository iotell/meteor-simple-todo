# README

### Preparing development

1. Clone the git-project to your workspace.
1. Run the command 'npm i' to install the node_modules
1. Run the command 'npm run start' to start the app. Start a browser on localhost:3000.
1. The package.json also has other scripts for debugging etc.

### About the app

Comments about the app.

- Not logged in users can see all tasks and change the completed status of tasks.
- Only logged in users can create new tasks.
- Tasks can only be deleted by the owned user.
- Private tasks can be viewed by others but not changed in any way.

### VSCode launchers

Here the launchers for vscode.

**launch.json**

```json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Meteor: Node",
      "runtimeExecutable": "npm",
      "runtimeArgs": ["run", "debug"],
      "outputCapture": "std",
      "port": 9229,
      "timeout": 30000
    },
    {
      "type": "chrome",
      "request": "launch",
      "name": "Meteor: Chrome",
      "url": "http://localhost:3000",
      "webRoot": "${workspaceFolder}"
    },
    {
      "type": "node",
      "request": "launch",
      "name": "Meteor: Server Tests",
      "runtimeExecutable": "npm",
      "runtimeArgs": ["run", "test-debug"],
      "port": 9229,
      "timeout": 60000
    },
    {
      "type": "chrome",
      "request": "launch",
      "name": "Meteor: Client Tests",
      "url": "http://localhost:3010",
      "webRoot": "${workspaceFolder}"
    }
  ],
  "compounds": [
    {
      "name": "Meteor: All",
      "configurations": ["Meteor: Chrome", "Meteor: Node"]
    },
    {
      "name": "Meteor: All Tests",
      "configurations": ["Meteor: Server Tests", "Meteor: Client Tests"]
    }
  ]
}
```
